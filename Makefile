jumpdrive_version := 0.8

.SILENT:
.PRECIOUS: jumpdrive/boot-%.img

# Devices
devices:
	echo "enchilada, beryllium-tianma, beryllium-ebbg"
enchilada:
	echo "oneplus-enchilada" > .device
beryllium-tianma:
	echo "xiaomi-beryllium-tianma" > .device
beryllium-ebbg:
	echo "xiaomi-beryllium-ebbg" > .device

# Flavours
flavours:
	echo "barebone, phosh, plasma-mobile"
barebone:
	echo "barebone" > .flavour
phosh:
	echo "phosh" > .flavour
plasma-mobile:
	echo "plasma-mobile" > .flavour


variables:
	$(eval DEVICE := $(shell cat .device))
	$(eval FLAVOUR := $(shell cat .flavour))
	if [ -z $(DEVICE) ]; then echo "Missing device"; exit 1; fi
	if [ -z $(FLAVOUR) ]; then echo "Missing flavour"; exit 1; fi

rootfs: variables
	rm -f .extra-packages
	if [[ "$(DEVICE)" == "oneplus-enchilada" ]]; then echo "sdm845-oneplus-enchilada" >> .extra-packages; fi
	if [[ "$(DEVICE)" == "xiaomi-beryllium-tianma" ]]; then echo "sdm845-xiaomi-beryllium-tianma" >> .extra-packages; fi
	if [[ "$(DEVICE)" == "xiaomi-beryllium-ebbg" ]]; then echo "sdm845-xiaomi-beryllium-ebbg" >> .extra-packages; fi
	if [[ "$(FLAVOUR)" == "barebone" ]]; then echo "" >> .extra-packages; fi
	if [[ "$(FLAVOUR)" == "phosh" ]]; then echo "" >> .extra-packages; fi
	if [[ "$(FLAVOUR)" == "plasma-mobile" ]]; then echo "" >> .extra-packages; fi
	echo "Building $(FLAVOUR) for $(DEVICE)"
	DEVICE=$(DEVICE) FLAVOUR=$(FLAVOUR) EXTRA_PACKAGES="$$(cat .extra-packages | tr "\n" " " | rev | cut -c2- | rev)" ./scripts/build.sh

inspect: variables
	DEVICE=$(DEVICE) FLAVOUR=$(FLAVOUR) ./scripts/inspect.sh

export: variables
	DEVICE=$(DEVICE) FLAVOUR=$(FLAVOUR) ./scripts/export.sh

ssh:
	ssh -o GlobalKnownHostsFile=/dev/null -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no kupfer@172.16.42.1

forwarding:
	./scripts/forwarding.sh

boot: variables
	if [[ "$(DEVICE)" == "oneplus-enchilada" ]]; then $(MAKE) boot-android-$(DEVICE)-$(FLAVOUR); fi
	if [[ "$(DEVICE)" == "xiaomi-beryllium-tianma" ]]; then $(MAKE) boot-android-$(DEVICE)-$(FLAVOUR); fi
	if [[ "$(DEVICE)" == "xiaomi-beryllium-ebbg" ]]; then $(MAKE) boot-android-$(DEVICE)-$(FLAVOUR); fi

boot-android-%: variables
	fastboot boot out/$*-boot.img


jumpdrive-push-emmc: variables
	DEVICE=$(DEVICE) FLAVOUR=$(FLAVOUR) ./scripts/jumpdrive-push.sh emmc

jumpdrive-inspect-emmc:
	./scripts/jumpdrive-inspect.sh emmc

jumpdrive-telnet:
	telnet 172.16.42.1

jumpdrive-boot: variables
	if [[ "$(DEVICE)" == "oneplus-enchilada" ]]; then $(MAKE) jumpdrive-boot-android-oneplus-enchilada; fi
	if [[ "$(DEVICE)" == "xiaomi-beryllium-tianma" ]]; then $(MAKE) jumpdrive-boot-android-xiaomi-beryllium-tianma; fi
	if [[ "$(DEVICE)" == "xiaomi-beryllium-ebbg" ]]; then $(MAKE) jumpdrive-boot-android-xiaomi-beryllium-ebbg; fi

jumpdrive-boot-android-%: jumpdrive/boot-%.img
	fastboot boot jumpdrive/boot-$*.img

jumpdrive/boot-%.img:
	mkdir -p jumpdrive
	wget https://github.com/dreemurrs-embedded/Jumpdrive/releases/download/$(jumpdrive_version)/boot-$*.img -O jumpdrive/boot-$*.img


cleanfast: variables
	rm -rf out/$(DEVICE)-$(FLAVOUR)*
	rm .extra-packages

clean:
	rm -rf out/
	rm -rf jumpdrive/
	rm .extra-packages
