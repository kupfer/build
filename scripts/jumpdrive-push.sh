#!/bin/bash

set -e

if [ -z "$DEVICE" ]; then
    echo "Missing device"
    exit 1
fi

if [ -z "$FLAVOUR" ]; then
    echo "Missing flavour"
    exit 1
fi

if [ "$#" -ne 1 ]; then
  echo "Usage: jumpdrive-push.sh <target>"
  echo "Targets: emmc, microsd"
  exit 1
fi

TARGET="$1"
if [[ "$TARGET" != "emmc" ]] && [[ "$TARGET" != "microsd" ]]; then
    echo "Target is $TARGET, but should be one of emmc, microsd"
    exit 1
fi

if [ "$(id -u)" -ne "0" ]; then
  echo "This script requires root."
  exit 1
fi

DEST=/mnt/kupfer
IMAGE_NAME="$DEVICE-$FLAVOUR"
ROOTFSIMG="out/$IMAGE_NAME-rootfs-minimal.img"
DRIVE=$(readlink -f "/dev/disk/by-id/$(ls /dev/disk/by-id/ | grep -i "${TARGET}_jumpdrive")")

mkdir -p "$DEST"

cleanup() {
  if [ -e "$DEST" ] && mountpoint "$DEST" >/dev/null; then
    umount -lc "$DEST" || true
  fi
  if [ -e "$DEST" ]; then
    rm -rf "$DEST" || true
  fi
}
trap cleanup EXIT

mount "$DRIVE" "$DEST"

mkdir -p "$DEST/.stowaways"
rsync -ah --progress "$ROOTFSIMG" "$DEST/.stowaways/kupfer.img"
