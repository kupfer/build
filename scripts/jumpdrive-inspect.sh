#!/bin/bash

set -e

if [ "$#" -ne 1 ]; then
  echo "Usage: jumpdrive-inspect.sh <target>"
  echo "Targets: emmc, microsd"
  exit 1
fi

TARGET="$1"
if [[ "$TARGET" != "emmc" ]] && [[ "$TARGET" != "microsd" ]]; then
    echo "Target is $TARGET, but should be one of emmc, microsd"
    exit 1
fi

if [ "$(id -u)" -ne "0" ]; then
  echo "This script requires root."
  exit 1
fi

DEST=/mnt/kupfer
DEST_ROOTFS=/mnt/kupfer-rootfs
DRIVE=$(readlink -f "/dev/disk/by-id/$(ls /dev/disk/by-id/ | grep -i "${TARGET}_jumpdrive")")

mkdir -p "$DEST" "$DEST_ROOTFS"

cleanup() {
  if [ -e "$DEST_ROOTFS" ] && mountpoint "$DEST_ROOTFS" >/dev/null; then
    umount -lc "$DEST_ROOTFS" || true
  fi
  if [ -e "$DEST_ROOTFS" ]; then
    rm -rf "$DEST_ROOTFS" || true
  fi
  if [ -e "$DEST" ] && mountpoint "$DEST" >/dev/null; then
    umount -lc "$DEST" || true
  fi
  if [ -e "$DEST" ]; then
    rm -rf "$DEST" || true
  fi
}
trap cleanup EXIT

mount "$DRIVE" "$DEST"
mount -o loop "$DEST/.stowaways/kupfer.img" "$DEST_ROOTFS"

echo "Inspect the image at $DEST_ROOTFS"

sleep infinity
