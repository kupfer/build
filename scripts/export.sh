#!/bin/bash

set -e

if [ -z "$DEVICE" ]; then
    echo "Missing device"
    exit 1
fi

if [ -z "$FLAVOUR" ]; then
    echo "Missing flavour"
    exit 1
fi

if [ "$(id -u)" -ne "0" ]; then
  echo "This script requires root."
  exit 1
fi

IMAGE_NAME="$DEVICE-$FLAVOUR"
ROOTFSIMG="out/$IMAGE_NAME-rootfs.img"

if [ ! -f "$ROOTFSIMG" ]; then
    echo "Missing rootfs"
    exit 1
fi

debugfs "$ROOTFSIMG" -R "dump /boot/Image out/$DEVICE-$FLAVOUR-Image"
debugfs "$ROOTFSIMG" -R "dump /boot/initramfs-linux.img out/$DEVICE-$FLAVOUR-initramfs.img"
debugfs "$ROOTFSIMG" -R "dump /boot/boot.img out/$DEVICE-$FLAVOUR-boot.img"
