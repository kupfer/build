#!/bin/bash

set -e

if [ -z "$DEVICE" ]; then
    echo "Missing device"
    exit 1
fi

if [ -z "$FLAVOUR" ]; then
    echo "Missing flavour"
    exit 1
fi

if [ "$(id -u)" -ne "0" ]; then
  echo "This script requires root."
  exit 1
fi

DEST=/mnt/kupfer
IMAGE_NAME="$DEVICE-$FLAVOUR"
ROOTFSIMG="out/$IMAGE_NAME-rootfs.img"

if [ ! -f "$ROOTFSIMG" ]; then
    echo "Missing rootfs"
    exit 1
fi

mkdir -p "$DEST"

cleanup() {
  if [ -e "$DEST" ] && mountpoint "$DEST" >/dev/null; then
    umount -lc "$DEST" || true
  fi
  if [ -e "$DEST" ]; then
    rm -rf "$DEST" || true
  fi
}
trap cleanup EXIT

mount -o loop "$ROOTFSIMG" "$DEST"

echo "Inspect the image at $DEST"

sleep infinity
