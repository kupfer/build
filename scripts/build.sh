#!/bin/bash

set -e

if [ -z "$DEVICE" ]; then
    echo "Missing device"
    exit 1
fi

if [ -z "$FLAVOUR" ]; then
    echo "Missing flavour"
    exit 1
fi

if [ "$(id -u)" -ne "0" ]; then
  echo "This script requires root."
  exit 1
fi

DEST=$(mktemp -d)
TMP=$(mktemp -d)
IMAGE_NAME="$DEVICE-$FLAVOUR"
ROOTFSIMG="out/$IMAGE_NAME-rootfs.img"
MINIMALROOTFSIMG="out/$IMAGE_NAME-rootfs-minimal.img"

mkdir -p out

cleanup() {
  if [ -e "$DEST" ] && mountpoint "$DEST" >/dev/null; then
    umount -lc "$DEST" || true
  fi
  if [ -e "$DEST" ]; then
    rm -rf "$DEST" || true
  fi
  if [ -e "$TMP" ]; then
    rm -rf "$TMP" || true
  fi
}
trap cleanup EXIT

function setup_rootfs() {
  if [ ! -f "$ROOTFSIMG" ]; then
    fallocate -l 5G "$ROOTFSIMG"
    mkfs.ext4 -L kupfer "$ROOTFSIMG"
  fi
  mount -o loop "$ROOTFSIMG" "$DEST"
}

function build_rootfs() {
  docker build -t archlinux:pacstrap - <src/Dockerfile

  cp src/pacman.conf "$TMP"
  echo "Server = http://mirror.archlinuxarm.org/\$arch/\$repo" >"$TMP"/mirrorlist

  if [[ "$LOCAL_MIRROR" == "" ]]; then
    # Could this break? Yes, but what now
    export LOCAL_MIRROR="http://$(ip route get 8.8.8.8 | head -1 | cut -d " " -f 7):8080/\$repo/\$arch"
  fi

  cp "$TMP/pacman.conf" "$TMP/pacman.conf.bak"
  cp "$TMP/mirrorlist" "$TMP/mirrorlist.bak"
  sed -i "s/Server = .*/Include = \/etc\/pacman\.d\/mirrorlist/" "$TMP/pacman.conf"
  printf "Server = %s" "$LOCAL_MIRROR" >"$TMP/mirrorlist"

  docker run \
    --privileged --rm -it \
    -v "$TMP/pacman.conf":/etc/pacman.conf \
    -v "$TMP/mirrorlist":/etc/pacman.d/mirrorlist \
    -v "$DEST":/newroot:z \
    archlinux:pacstrap \
    pacstrap -c -G -M /newroot base base-kupfer $(printf " %s" "${EXTRA_PACKAGES[@]}") --needed --overwrite=* -yyuu

  cp "$TMP/pacman.conf"* "$DEST/etc/"
  cp "$TMP/mirrorlist"* "$DEST/etc/pacman.d/"

  docker run \
    --privileged --rm -i \
    -v "$DEST":/newroot:z \
    archlinux:pacstrap \
    arch-chroot /newroot <<EOF
if ! id -u "kupfer" >/dev/null 2>&1; then
  useradd -m kupfer
fi
usermod -a -G network,video,audio,optical,storage,input,scanner,games,lp,rfkill,wheel kupfer
echo "kupfer:123456" | chpasswd
chown kupfer:kupfer /home/kupfer -R
EOF
}

function shrink_rootfs() {
  cp "$ROOTFSIMG" "$MINIMALROOTFSIMG"
  e2fsck -fy "$MINIMALROOTFSIMG"
  resize2fs -M "$MINIMALROOTFSIMG"
}

function setup_qemu() {
  # Remove current configuration
  if [ -f /proc/sys/fs/binfmt_misc/qemu-aarch64 ]; then
    echo -1 >/proc/sys/fs/binfmt_misc/qemu-aarch64
  fi
  echo ':qemu-aarch64:M:0:\x7f\x45\x4c\x46\x02\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\xb7\x00:\xff\xff\xff\xff\xff\xff\xff\x00\xff\xff\xff\xff\xff\xff\xff\xff\xfe\xff\xff\xff:/usr/bin/qemu-aarch64-static:CF' >/proc/sys/fs/binfmt_misc/register
}

setup_qemu

setup_rootfs
build_rootfs

cleanup
shrink_rootfs
